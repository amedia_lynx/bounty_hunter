#!/usr/bin/env groovy

@Grab(group = 'net.sourceforge.htmlcleaner', module = 'htmlcleaner', version = '2.13')
import javax.xml.xpath.*;
import org.htmlcleaner.*;

def cli = new CliBuilder();
cli.with {
    p longOpt: 'points',   args: 1, required: true, 'specify minimum bounty points'
    v longOpt: 'votes',    args: 1, required: true, 'specify minimum vote count'
    a longOpt: 'answers',  args: 1, required: true, 'specify maximum answer count'
    m longOpt: 'mainTag',  args: 1, required: true, 'specify primary tag'
    t longOpt: 'tags',     args: 1, required: true, 'specify matching tags (separated by commas)'
}

def options = cli.parse(args)
if (!options) { return }
if (options.h) { cli.usage(); return }

def tagsExpr   = options.t.split(',').collect({ "contains(., '${it}')" }).join(' or ')
def highPoints = "./div[@class='bounty-indicator' and number(substring-after(.,'+'))>=${options.p}]"
def highVotes  = "preceding-sibling::div//span[contains(@class, 'vote-count-post') and number(.)>=${options.v}]"
def lowAnswers = "preceding-sibling::div//div[contains(@class, 'status') and ./strong[number(.)<=${options.a}]]"
def hasTags    = ".//div[starts-with(@class, 'tags')]/a[${tagsExpr}]"
def xpath      = "//div[@class='summary' and ${highPoints} and ${highVotes} and ${lowAnswers} and ${hasTags}]//h3/a"

def url        = "http://stackoverflow.com/questions/tagged/${options.m}?sort=featured&pageSize=50".toURL()
def dom        = new DomSerializer(new CleanerProperties()).createDOM(new HtmlCleaner().clean(url))
def result     = XPathFactory.newInstance().newXPath().evaluate(xpath, dom, XPathConstants.NODESET)

println 'Found ' + "${result.length}".padLeft(2, '0') + ' matches at ' + new Date().format('YYYY-MM-dd hh:mm a')
println ''.padLeft(39, '=')
if (result.length > 0) {
    (0..(result.length-1)).each {
        println '\n"' + result.item(it).textContent + '"'
        println 'http://stackoverflow.com' + result.item(it).getAttribute('href') + '\n'
    }
}
