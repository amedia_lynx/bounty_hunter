#!/usr/local/bin/node

var program   = require('commander');
var format    = require('string-template');
var moment    = require('moment');
var webdriver = require('selenium-webdriver');
var browser   = new webdriver.Builder().withCapabilities({"browserName": "chrome"}).build();
var pad       = function (num, size) { return ('000000000' + num).substr(-size); };

program
	.option('-p, --points <n>',      'specify minimum bounty points')
	.option('-v, --votes <n>',       'specify minimum vote count')
	.option('-a, --answers <n>',     'specify maximum answer count')
	.option('-m, --mainTag <value>', 'specify primary tag')
	.option('-t, --tags <items>',    'specify matching tags (separated by commas)')
	.parse(process.argv);

var tags = program.tags.split(',').map(function(tag) { return format('contains(., "{0}")', tag); }).join(' or ');

var isHighPoints = './div[@class="bounty-indicator" and number(substring-after(.,"+"))>={0}]';
var isHighVotes  = 'preceding-sibling::div//span[contains(@class, "vote-count-post") and number(.)>={0}]';
var isLowAnswers = 'preceding-sibling::div//div[contains(@class, "status") and ./strong[number(.)<={0}]]';
var isHavingTags = './/div[starts-with(@class, "tags")]/a[{0}]';
var xpathTmpl    = '//div[@class="summary" and {0} and {1} and {2} and {3}]//h3/a';
var finalXpath   = format(xpathTmpl, format(isHighPoints, program.points), format(isHighVotes, program.votes),
	format(isLowAnswers, program.answers), format(isHavingTags, tags));
var url          = 'http://stackoverflow.com/questions/tagged/{0}?sort=featured&pageSize=50';

browser.get(format(url, program.mainTag));
browser.findElements(webdriver.By.xpath(finalXpath)).then(function (array) {
	console.log("\nFound " + pad(array.length, 2) + " match(es) at " + moment(new Date()).format('YYYY-MM-DD HH:mm'));
	console.log("======================================\n");
	var ctr = 0;

	array.forEach(function (node) {
		node.getText().then(function (text) {
			console.log('"' + text + '"');
			return node.getAttribute('href');
		}).then(function (href) {
			console.log(href + '\n');
			if (++ctr === array.length) {
				browser.quit();
			}
		});
	});
});
