# Bounty Hunter #

This is a showcase application, illustrating how XPATH might be used to perform data extraction from Stack Overflow website.

### Scenario ###

Assuming that you're one of the registered StackOverflow users, who wants to periodically check for questions up for bounty, which match the following criteria:

* high bounty points
* high vote count
* low (existing) answers
* is one of your areas of expertise

Then, you can use this project to query for the questions that matched the above criteria.

### How to run ###

This program actually comes in two flavors: `node.js` and `groovy` script.

If you're running a `node.js`

* `npm install`
* make sure you have `chromedriver` available on your `PATH`
* `./bounty-hunter.js --help` to see the available options

If you're running a `groovy` script

* make sure you have `java` and `groovy` installed
* `./bounty-hunter.groovy --help` to see the available options

Note that both of these will be run as bash script, and you *might* need to adjust [`shebang`](https://en.wikipedia.org/wiki/Shebang_(Unix)) at the top of each file to point to the correct location of the executable.

### Example ###
for `node.js`:

`./bounty_hunter.js -p 100 -v 0 -a 10 -m angularjs -t javascript,html5`

for `groovy`:

`./bounty_hunter.groovy -p 100 -v 0 -a 10 -m angularjs -t javascript,html5`

### Final note ###
Please note that we can improve this program a couple of ways, for example:

* in `node.js` version, we can avoid `WebDriver` by operating on the `html` file directly, though it can be [more involving](http://stackoverflow.com/a/25971812/3788115).
* this program doesn't yet fetch for data periodically, though you can achieve that with `timeout()`, or `cron` (if you're on *nix system)

These improvements are left as users' exercise. ;-)